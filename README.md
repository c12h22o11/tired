<a href='https://gitee.com/c12h22o11/tired'><img align="right" src='https://gitee.com/c12h22o11/tired/widgets/widget_1.svg?color=undefined' alt='Fork me on Gitee'></img></a>

# 疲劳检测

#### 采用了dlib等其余开源库
#### 基于OpenCV的驾驶员的疲劳检测

## 开发环境


- OS: 
  - WSL2_ubuntu 不可以使用摄像头及串口
  - Ubuntu20.04 TLS
  - Windows，只可以使用Python版本代码
  - Jetson Nano
<br>

- IDE:
   前期使用Pycharm CE进行Python开发
   后期使用VSC进行Python和C++双语言版本开发
<br>

- depends:
   - OpenCV 
   - dlib
   - Cmake
   - SPDLOG

## 获取代码方式

```shell
git clone https://gitee.com/c12h22o11/tired.git
```

## 编译方式
```shell
cd cpp/build/
cmake ..
make -j 8
sudo make install
```

## 流程图

### 疲劳驾驶检测流程图

![驾驶员流程图](/image/mindmap_update.png)

### 架构图

![架构图](/image/系统框架.png)

### 硬件流程图

![硬件层](/image/hardware_update.png)

## 文档树

```
┌─────────────────────────────────────────────
│ Tired
│ 疲劳驾驶检测
├─────────────────────────────────────────────
│
├─build              //Cmake编译产生文件         
│
├─cpp                //C++源码              
│ 
├─data               //数据集
│ 
├─image              //测试用图像部分
│
├─python             //Python面向对象源码
│ 
├─python_            //Python面向结构源码
│ 
├─Readme.md          //说明文档                 
│
└─────────────────────────────────────────────
```

<br>

## TODO

1. 增加人体姿态模块
2. 优化加速
   - 优化过程
   - 优化渲染模式
   - 优化代码核心
3. 移植代码
4. 添加时域分析

<br><br>

[![C12H22O11/Tired](https://gitee.com/c12h22o11/tired/widgets/widget_card.svg?colors=db4b7b,ffffff,ffffff,e3e9ed,783c78,9b9b9b)](https://gitee.com/c12h22o11/tired)