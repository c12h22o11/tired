import numpy as np
import cv2 as cv
import dlib

predictor = dlib.shape_predictor(
    r'./data/shape_predictor_68_face_landmarks.dat')
detector = dlib.get_frontal_face_detector()
face_dict = dict([
    ("mouth", (48, 68)),
    ("right_eyebrow", (17, 22)),
    ("left_eyebrow", (22, 27)),
    ("right_eye", (36, 42)),
    ("left_eye", (42, 48)),
    ("nose", (27, 36)),
    ("jaw", (0, 17))
])

red = (0, 0, 255)
green = (0, 255, 0)
yellow = (0, 255, 255)
blue = (255, 0, 0)
white = (255, 255, 255)
black = (0, 0, 0)

font_sim = cv.FONT_HERSHEY_SIMPLEX
font_sim_s = cv.FONT_HERSHEY_PLAIN
font_com = cv.FONT_HERSHEY_COMPLEX
font_com_s = cv.FONT_HERSHEY_COMPLEX_SMALL
font_hand = cv.FONT_HERSHEY_SCRIPT_SIMPLEX

pts_nums = [17, 21, 22, 26, 36, 39, 42, 45, 31, 35, 48, 54, 57, 8]
# 世界坐标系(UVW)

"""
# 33左眉左上角
# 29左眉右角
# 34右眉左角
# 38右眉右上角
# 13左眼左上角
# 17左眼右上角
# 25右眼左上角
# 21右眼右上角
# 55鼻子左上角
# 49鼻子右上角
# 43嘴左上角
# 39嘴右上角
# 45嘴中央下角
# 6下巴角
"""
object_pts = np.float32([[6.825897, 6.760612, 4.402142],
                         [1.330353, 7.122144, 6.903745],
                         [-1.330353, 7.122144, 6.903745],
                         [-6.825897, 6.760612, 4.402142],
                         [5.311432, 5.485328, 3.987654],
                         [1.789930, 5.393625, 4.413414],
                         [-1.789930, 5.393625, 4.413414],
                         [-5.311432, 5.485328, 3.987654],
                         [2.005628, 1.409845, 6.165652],
                         [-2.005628, 1.409845, 6.165652],
                         [2.774015, -2.080775, 5.048531],
                         [-2.774015, -2.080775, 5.048531],
                         [0.000000, -3.116408, 6.097667],
                         [0.000000, -7.415691, 4.070434]])

# 重新投影3D点的世界坐标轴以验证结果姿势
reprojectsrc = np.float32([[10.0, 10.0, 10.0],
                           [10.0, 10.0, -10.0],
                           [10.0, -10.0, -10.0],
                           [10.0, -10.0, 10.0],
                           [-10.0, 10.0, 10.0],
                           [-10.0, 10.0, -10.0],
                           [-10.0, -10.0, -10.0],
                           [-10.0, -10.0, 10.0]])
# 绘制正方体12轴
line_pairs = [[0, 1], [1, 2], [2, 3], [3, 0],
              [4, 5], [5, 6], [6, 7], [7, 4],
              [0, 4], [1, 5], [2, 6], [3, 7]]


def shape_to_np(shape, type_="int"):
    # 创建68*2
    coord = np.zeros((shape.num_parts, 2), dtype=type_)
    # 遍历每一个关键点
    # 得到坐标
    for i in range(0, shape.num_parts):
        coord[i] = (shape.part(i).x, shape.part(i).y)
    return coord


def show(image, p, is_wait=False):
    """
    展示函数
    :param image: 图像
    :param p: 缩放比
    :param is_stop: 是否停留
    :return:
    """
    # (h, w) = image.shape[:2]
    # image = cv.resize(image, (int(w * p), int(h * p)))
    cv.imshow("show", image)
    if is_wait:
        cv.waitKey()
		
def decode_fourcc(cc):
	return "".join([chr((int(cc) >> 8 * i) & 0xFF) for i in range(4)])
