import cv2 as cv
import numpy as np
import face
import functions as fun

# 相机坐标系(XYZ)：添加相机内参
K = [6.5308391993466671e+002, 0.0, 3.1950000000000000e+002,
     0.0, 6.5308391993466671e+002, 2.3950000000000000e+002,
     0.0, 0.0, 1.0]  # 等价于矩阵[fx, 0, cx; 0, fy, cy; 0, 0, 1]
# 图像中心坐标系(uv)：相机畸变参数[k1, k2, p1, p2, k3]
D = [7.0834633684407095e-002, 6.9140193737175351e-002,
     0.0, 0.0, -1.3073460323689292e+000]


class Estimator(face.Face):
    __shape = []
    __cam_mat = []
    __distor_coff = []
    image_pts = []

    def cal(self, gray, rect):
        self.__face = fun.shape_to_np(fun.predictor(gray, rect), "int")
        shape = self.__face
        self.image_pts = np.float32([shape[17], shape[21], shape[22], shape[26], shape[36],
                                     shape[39], shape[42], shape[45], shape[31], shape[35],
                                     shape[48], shape[54], shape[57], shape[8]])
        self.__shape = np.array(self.__shape).astype(np.float32)
        self.__cam_mat = np.array(K).reshape(3, 3).astype(np.float32)
        self.__distor_coff = np.array(D).reshape(5, 1).astype(np.float32)

    def calibration(self, frame, inter_corner_shape=(9, 6), size_per_grid=0.14):
        # criteria: only for subpix calibration, which is not used here.
        # criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
        w,h = inter_corner_shape
        # cp_int: corner point in int form, save the coordinate of corner points in world sapce in 'int' form
        # like (0,0,0), (1,0,0), (2,0,0) ....,(10,7,0).
        cp_int = np.zeros((w*h,3), np.float32)
        cp_int[:,:2] = np.mgrid[0:w,0:h].T.reshape(-1,2)
        # cp_world: corner point in world space, save the coordinate of corner points in world space.
        cp_world = cp_int*size_per_grid

        obj_points = [] # the points in world space
        img_points = [] # the points in image space (relevant to obj_points)

        gray_img = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        # find the corners, cp_img: corner points in pixel space.
        ret, cp_img = cv.findChessboardCorners(gray_img, (w,h), None)
        # if ret is True, save.
        if True == ret:
            # cv.cornerSubPix(gray_img,cp_img,(11,11),(-1,-1),criteria)
            obj_points.append(cp_world)
            img_points.append(cp_img)
            # view the corners
            cv.drawChessboardCorners(frame, (w,h), cp_img, ret)
            cv.imshow('FoundCorners', frame)
            cv.waitKey(1)
        cv.destroyAllWindows()
        # calibrate the camera
        ret, mat_inter, coff_dis, v_rot, v_trans = cv.calibrateCamera(obj_points, img_points, gray_img.shape[::-1], None, None)

        self.__cam_mat = mat_inter
        self.__distor_coff = coff_dis

    def estimator(self, img):
        # print("[DEBUG] : self.image_pts is ", self.image_pts)
        _, vec_R, vec_T = cv.solvePnP(
            fun.object_pts, self.image_pts, self.__cam_mat, self.__distor_coff)
        reprojectdst, _ = cv.projectPoints(
            fun.reprojectsrc, vec_R, vec_T, self.__cam_mat, self.__distor_coff)
        reprojectdst = tuple(map(tuple, reprojectdst.reshape(8, 2)))
        mat_R, _ = cv.Rodrigues(vec_R)
        pose = cv.hconcat((mat_R, vec_T))
        _, _, _, _, _, _, euler_angle = cv.decomposeProjectionMatrix(pose)
        return reprojectdst, euler_angle

    def draw(self, frame):
        size = frame.shape
        width = size[1]
        reprojectdst, euler_angle = self.estimator(frame)
        for start, end in fun.line_pairs:
            # print("reprojectdst[start] : ", reprojectdst[start])
            cv.line(frame, reprojectdst[start], reprojectdst[end], fun.green, thickness=2)
            # 显示角度结果
        cv.putText(frame, "X: " + "{:3.2f}".format(
            euler_angle[0, 0]), (width - 100, 20), cv.FONT_HERSHEY_SIMPLEX, 0.75, fun.blue, thickness=2)
        cv.putText(frame, "Y: " + "{:3.2f}".format(
            euler_angle[1, 0]), (width - 100, 50), cv.FONT_HERSHEY_SIMPLEX, 0.75, fun.blue, thickness=2)
        cv.putText(frame, "Z: " + "{:3.2f}".format(
            euler_angle[2, 0]), (width - 100, 80), cv.FONT_HERSHEY_SIMPLEX, 0.75, fun.blue, thickness=2)
        return frame