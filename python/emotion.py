from re import S
import cv2 as cv
import functions as fun
from scipy.spatial import distance as dist
import face


class Emotion(face.Face):
    __isTired = False
    __ear = 0
    __mar = 0

    thresh_eye = 0.27
    thresh_mouth = 0.3
    thresh_frame_eye = 5
    thresh_frame_mouth = 10

    def cal(self, gray, rect):
        self.__face = fun.shape_to_np(fun.predictor(gray, rect), "int")
        self.__leftEye = self.__face[fun.face_dict["left_eye"]
                                     [0]:fun.face_dict["left_eye"][1]]
        self.__rightEye = self.__face[fun.face_dict["right_eye"]
                                      [0]:fun.face_dict["right_eye"][1]]
        self.__mouth = self.__face[fun.face_dict["mouth"]
                                   [0]:fun.face_dict["mouth"][1]]
        self.__ear = (self.EAR(True) + self.EAR(False)) / 2
        self.__mar = self.MAR()
        self.__isTired = False

    def thresh(self):
        self.thresh_eye = 0.27
        self.thresh_mouth = 0.18
        self.thresh_frame_eye = 5
        self.thresh_frame_mouth = 3

    def EAR(self, option):
        if option:
            eye = self.__leftEye
        else:
            eye = self.__rightEye
        # 计算距离，竖直的
        A = dist.euclidean(eye[1], eye[5])
        B = dist.euclidean(eye[2], eye[4])
        # 计算距离，水平的
        C = dist.euclidean(eye[0], eye[3])
        return (A + B) / (2.0 * C)

    def MAR(self):
        mouth = self.__mouth
        mouth_index = 49
        # 计算距离，竖直的
        A = dist.euclidean(mouth[68 - mouth_index], mouth[62 - mouth_index])
        B = dist.euclidean(mouth[66 - mouth_index], mouth[64 - mouth_index])
        # 计算距离，水平的
        C = dist.euclidean(mouth[55 - mouth_index], mouth[49 - mouth_index])
        # ear值
        return (A + B) / (2.0 * C)

    def draw(self, frame):
        size = frame.shape
        height = size[0]
        cv.drawContours(frame, [cv.convexHull(
            self.__leftEye)], -1, fun.green, 1)
        cv.drawContours(frame, [cv.convexHull(
            self.__rightEye)], -1, fun.green, 1)
        cv.drawContours(frame, [cv.convexHull(self.__mouth)], -1, fun.green, 1)

        str_mar = "The mouth is close"
        str_ear = "The eye is open"
        str_isTired = "Not tired yet"
        if self.__mar > self.thresh_mouth:
            str_mar = "The mouth is too big"
        if self.__ear < self.thresh_eye:
            str_ear = "The eyes are too small"
        if self.CalTired():
            str_isTired = "Tired"

        cv.putText(frame, "EAR: {:.2f}".format(self.__ear),
                   (30, 30), 0, 0.7, fun.yellow, 2)
        cv.putText(frame, "MAR: {:.2f}".format(self.__mar),
                   (30, 60), 0, 0.7, fun.yellow, 2)
        cv.putText(frame, str_ear, (30, height - 90), 3, 0.7, fun.white, 2)
        cv.putText(frame, str_mar, (30, height - 60), 3, 0.7, fun.white, 2)
        cv.putText(frame, str_isTired, (30, height - 30), 0, 0.9, fun.white, 2)

        return frame

    def CalTired(self):
        count_eye = self.get(4)
        count_mouth = self.get(5)

        if self.__ear < self.thresh_eye:
            count_eye += 1
            print("[DEBUG]", count_eye)
            self.set(0, count_eye)

        if count_eye > self.thresh_frame_eye:
            self.__isTired = True
            self.set(0, 0)

        if self.__mar > self.thresh_mouth:
            count_mouth += 1
            print("[DEBUG]", count_mouth)
            self.set(1, count_mouth)

        if count_mouth > self.thresh_frame_mouth:
            self.__isTired == True
            self.set(1, 0)
        
        return self.__isTired
