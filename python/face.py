import cv2 as cv
from scipy.spatial import distance as dist
import functions as fun


class Face:
    __face = []
    __leftEye = []
    __rightEye = []
    __mouth = []
    
    __count_eye = 0
    __count_mouth = 0

    def cal(self, gray, rect):
        self.__face = fun.shape_to_np(fun.predictor(gray, rect), "int")
        self.__leftEye = self.__face[fun.face_dict["left_eye"]
                                     [0]:fun.face_dict["left_eye"][1]]
        self.__rightEye = self.__face[fun.face_dict["right_eye"]
                                      [0]:fun.face_dict["right_eye"][1]]
        self.__mouth = self.__face[fun.face_dict["mouth"]
                                   [0]:fun.face_dict["mouth"][1]]


    def get(self, option):
        """[得到值]

        Args:
            option ([int]): [options for select value to read]
            1 : lefteye 
            2 : righteye 
            3 : mouth
            4 : count_eye
            5 : count_mouth

        Returns:
            [int]: [对应的值]
        """
        if option == 1:
            return self.__leftEye
        elif option == 2:
            return self.__rightEye
        elif option == 3:
            return self.__mouth
        elif option == 4:
            return self.__count_eye
        else:
            return self.__count_mouth

    def set(self, option, var):
        """[设置值]

        Args:
            option ([int]): [options for select value to store]
            0 : eye
            1 : mouth 
        """
        if option == 0: 
            self.__count_eye = var
        else:
            self.__count_mouth = var
