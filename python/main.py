from numpy.lib.function_base import gradient
from estimator import Estimator
from emotion import Emotion
import cv2 as cv
import functions as fun

fact = 0.7

file_read = r"E:\\QQ数据\\3130676972\\Video\\zwy.mp4"
file_write = r"D:\\CHO\\out.mp4"

cap = cv.VideoCapture(file_read)

fps = cap.get(cv.CAP_PROP_FPS)
size = (int(cap.get(cv.CAP_PROP_FRAME_WIDTH) * fact),
        int(cap.get(cv.CAP_PROP_FRAME_HEIGHT) * fact))
delta = 1000 / fps
code = cv.VideoWriter_fourcc(*fun.decode_fourcc(cap.get(cv.CAP_PROP_FOURCC)))

writer = cv.VideoWriter(file_write, code, fps, size)

if not cap.isOpened():
    exit(0)

face_object_emotion = Emotion()
face_object_estimator = Estimator()

while True:
    ret, frame = cap.read(0)
    if frame is None:
        print("[WARN]: frame is null !!!")
        break

    frame = cv.flip(frame, 1)
    #frame = cv.resize(frame, (720, 432))
    frame = cv.resize(frame, size, interpolation=cv.INTER_AREA)
    gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)

    rects = fun.detector(gray, 0)
    if 0 != len(rects):
        for rect in rects:
            face_object_emotion.cal(gray, rect)
            result = face_object_emotion.draw(frame)
            face_object_estimator.cal(gray, rect)
            result = face_object_estimator.draw(result)
            writer.write(result)
    else:
        print("[error] can't find aim")

    fun.show(frame, 1)
    if ord('q') == cv.waitKey(int(delta)):
        break

cap.release()
