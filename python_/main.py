from numpy.lib.function_base import gradient
import cv2 as cv
import function as fun
import face_

fact = 3

cap = cv.VideoCapture(0)  # , cv.CAP_DSHOW)
fps = cap.get(cv.CAP_PROP_FPS)
size = (int(cap.get(cv.CAP_PROP_FRAME_WIDTH) * fact),
        int(cap.get(cv.CAP_PROP_FRAME_HEIGHT) * fact))
delta = 1000 / fps

if not cap.isOpened():
    exit(0)

while True:
    ret, frame = cap.read()
    if frame is None:
        print("[WARN]: frame is null !!!")
        break

    frame = cv.flip(frame, 1)
    frame = cv.resize(frame, (720, 540))
    # frame = cv.resize(frame, size, interpolation=cv.INTER_AREA)
    gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)

    rects = fun.detector(gray, 0)
    if 0 != len(rects):
        for rect in rects:
            # face_object = Emotion(gray, rect)
            # face_object = face_.(gray, rect)
            result = face_.draw(frame)
    else:
        print("[error] can't find aim")

    fun.show(frame, 1)
    if ord('q') == cv.waitKey(int(delta)):
        break

cap.release()
