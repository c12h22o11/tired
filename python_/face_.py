from re import S
import cv2 as cv
import function as fun
from scipy.spatial import distance as dist
import numpy as np

left_eye_ = []
right_eye_ = []
mouth_ = []
face_ = []
mar_ = 0
ear_ = 0
isTired_ = False
thresh_mouth = 0
thresh_eye = 0
cam_mat_ = []
image_pts_ = []
dis_coff_ = []


def init(gray, rect):
    face_ = fun.shape_to_np(fun.predictor(gray, rect), "int")
    left_eye_ = face_[fun.face_dict["left_eye"]
                      [0]:fun.face_dict["left_eye"][1]]
    right_eye_ = face_[fun.face_dict["right_eye"]
                       [0]:fun.face_dict["right_eye"][1]]
    mouth_ = face_[fun.face_dict["mouth"]
                   [0]:fun.face_dict["mouth"][1]]
    mar_ = MAR(mouth_)
    ear_ = (EAR(right_eye_) + EAR(left_eye_)) / 2


def calibration(frame, inter_corner_shape=(11, 8), size_per_grid=0.02):
    # criteria: only for subpix calibration, which is not used here.
    # criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)
    w, h = inter_corner_shape
    # cp_int: corner point in int form, save the coordinate of corner points in world sapce in 'int' form
    # like (0,0,0), (1,0,0), (2,0,0) ....,(10,7,0).
    cp_int = np.zeros((w*h, 3), np.float32)
    cp_int[:, :2] = np.mgrid[0:w, 0:h].T.reshape(-1, 2)
    # cp_world: corner point in world space, save the coordinate of corner points in world space.
    cp_world = cp_int * size_per_grid

    obj_points = []  # the points in world space
    img_points = []  # the points in image space (relevant to obj_points)

    gray_img = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    # find the corners, cp_img: corner points in pixel space.
    ret, cp_img = cv.findChessboardCorners(gray_img, (w, h), None)
    # if ret is True, save.
    if True == ret:
        # cv.cornerSubPix(gray_img,cp_img,(11,11),(-1,-1),criteria)
        obj_points.append(cp_world)
        img_points.append(cp_img)
        # view the corners
        cv.drawChessboardCorners(frame, (w, h), cp_img, ret)
        # cv.imshow('FoundCorners', frame)
        cv.waitKey(1)

    # calibrate the camera
    ret, mat_inter, coff_dis, v_rot, v_trans = cv.calibrateCamera(
        obj_points, img_points, gray_img.shape[::-1], None, None)
    # print(("ret:"), ret)
    # print(("internal matrix:\n"), mat_inter)
    # in the form of (k_1,k_2,p_1,p_2,k_3)
    # print(("distortion cofficients:\n"), coff_dis)
    # print(("rotation vectors:\n"), v_rot)
    # print(("translation vectors:\n"), v_trans)
    
    # calculate the error of reproject
    total_error = 0
    for i in range(len(obj_points)):
        img_points_repro, _ = cv.projectPoints(
            obj_points[i], v_rot[i], v_trans[i], mat_inter, coff_dis)
        error = cv.norm(img_points[i], img_points_repro,
                        cv.NORM_L2)/len(img_points_repro)
        total_error += error
    # print(("Average Error of Reproject: "), total_error/len(obj_points))

    # return mat_inter, coff_dis
    cam_mat_ = mat_inter
    dis_coff_ = coff_dis


def EAR(eye):
    # 计算距离，竖直的
    A = dist.euclidean(eye[1], eye[5])
    B = dist.euclidean(eye[2], eye[4])
    # 计算距离，水平的
    C = dist.euclidean(eye[0], eye[3])
    return (A + B) / (2.0 * C)


def MAR(mouth):
    mouth_index = 49
    # 计算距离，竖直的
    A = dist.euclidean(mouth[68 - mouth_index], mouth[62 - mouth_index])
    B = dist.euclidean(mouth[66 - mouth_index], mouth[64 - mouth_index])
    # 计算距离，水平的
    C = dist.euclidean(mouth[55 - mouth_index], mouth[49 - mouth_index])
    # ear值
    return (A + B) / (2.0 * C)


def solve():
    _, vec_R, vec_T = cv.solvePnP(
        fun.object_pts, image_pts_, cam_mat_, dis_coff_)
    reprojectdst, _ = cv.projectPoints(
        fun.reprojectsrc, vec_R, vec_T, cam_mat_, dis_coff_)
    reprojectdst = tuple(map(tuple, reprojectdst.reshape(8, 2)))
    mat_R, _ = cv.Rodrigues(vec_R)
    pose = cv.hconcat((mat_R, vec_T))
    _, _, _, _, _, _, euler_angle = cv.decomposeProjectionMatrix(pose)
    return reprojectdst, euler_angle


def draw(frame):
    reprojectdst, euler_angle = solve()

    str_mar = "The mouth is close"
    str_ear = "The eye is open"
    str_isTired = "Not tired yet"

    if mar_ > thresh_mouth:
        str_mar = "The mouth is too big"
    if ear_ < thresh_eye:
        str_ear = "The eyes are too small"
    if isTired_:
        str_isTired = "tired"

    cv.drawContours(frame, [cv.convexHull(
        face_[fun.face_dict["left_eye"][0]:fun.face_dict["left_eye"][1]])], -1, fun.green, 2)
    cv.drawContours(frame, [cv.convexHull(
        face_[fun.face_dict["right_eye"][0]:fun.face_dict["right_eye"][1]])], -1, fun.green, 2)
    cv.drawContours(frame, [cv.convexHull(
        face_[fun.face_dict["mouth"][0]:fun.face_dict["mouth"][1]])], -1, fun.green, 2)
    for start, end in fun.line_pairs:
        cv.line(frame, reprojectdst[start], reprojectdst[end], fun.blue)
    cv.putText(frame, "EAR: {:.2f}".format(ear_),
               (300, 30), fun.font_sim, 0.7, fun.yellow, 2)
    cv.putText(frame, "MAR: {:.2f}".format(mar_),
               (500, 30), fun.font_sim, 0.7, fun.yellow, 2)
    cv.putText(frame, str_ear, (50, 30), fun.font_com, 0.9, fun.green, 2)
    cv.putText(frame, str_mar, (50, 80), fun.font_com, 0.9, fun.green, 2)
    cv.putText(frame, str_isTired, tuple(
        face_[8].tolist()), 0, 0.9, fun.green, 2)
    cv.putText(frame, "X: " + "{:7.2f}".format(
        euler_angle[0, 0]), (20, 20), fun.font_sim, 0.75, fun.green, 2)
    cv.putText(frame, "Y: " + "{:7.2f}".format(
        euler_angle[1, 0]), (20, 50), fun.font_sim, 0.75, fun.green, 2)
    cv.putText(frame, "Z: " + "{:7.2f}".format(
        euler_angle[2, 0]), (20, 80), fun.font_sim, 0.75, fun.green, 2)

    return frame

def cal():
    pass