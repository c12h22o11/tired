#include "face.hpp"

#include <map>

#include "config.hpp"
#include "spdlog/spdlog.h"

Face::Face() { SPDLOG_DEBUG("Face constructed."); }

Face::Face(std::vector<cv::Point2f> face_pts) {
  SPDLOG_DEBUG("Face constructing.");

  face_ = face_pts;
  for (int i = dict::right_eye.x; i < dict::right_eye.y; i++)
    right_eye_.emplace_back(face_pts[i]);
  for (int i = dict::left_eye.x; i < dict::left_eye.y; i++)
    left_eye_.emplace_back(face_pts[i]);
  for (int i = dict::mouth.x; i < dict::mouth.y; i++)
    mouth_.emplace_back(face_pts[i]);

  SPDLOG_DEBUG("Face constructed.");
}

Face::~Face() { SPDLOG_DEBUG("Face destructed."); }

cv::Point2f Face::GetPoint(int num) { return face_[num]; }

std::vector<cv::Point2f> Face::GetPoints(int option) {
  std::vector<cv::Point2f> vec;
  if (option == 0) {
    vec = mouth_;
  } else if (option == 1) {
    vec = left_eye_;
  } else if (option == 2) {
    vec = right_eye_;
  } else
    vec = face_;
  return vec;
}

void Face::SetFace(std::vector<cv::Point2f> face_pts){
  face_ = face_pts;
  for (int i = dict::right_eye.x; i < dict::right_eye.y; i++)
    right_eye_.emplace_back(face_pts[i]);
  for (int i = dict::left_eye.x; i < dict::left_eye.y; i++)
    left_eye_.emplace_back(face_pts[i]);
  for (int i = dict::mouth.x; i < dict::mouth.y; i++)
    mouth_.emplace_back(face_pts[i]);
  SPDLOG_DEBUG("success SetFace");
}