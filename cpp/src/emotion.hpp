#pragma once

#include <string>
#include <vector>

#include "face.hpp"
#include "opencv2/opencv.hpp"

class Emotion : private Face {
 private:
  bool tired_;
  double ear_;
  double mar_;

  float thresh_eye_;
  float thresh_mouth_;
  float frame_eye_;
  float frame_mouth_;

  void EAR();
  void MAR();
  bool Cal();
  void Thresh();
  double Dist(cv::Point2f a, cv::Point2f b);

 public:
  Emotion();
  Emotion(std::vector<cv::Point2f> face_pts);
  ~Emotion();

  double GetEAR();
  double GetMAR();
  bool GetTired();
  void Draw(cv::Mat& frame);
};