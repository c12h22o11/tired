#pragma once

#include <vector>

#include "opencv2/opencv.hpp"

class Face {
 private:
  std::vector<cv::Point2f> face_;
  std::vector<cv::Point2f> left_eye_;
  std::vector<cv::Point2f> right_eye_;
  std::vector<cv::Point2f> mouth_;

 public:
  Face();
  Face(std::vector<cv::Point2f> face_pts);
  ~Face();

  void SetFace(std::vector<cv::Point2f> face_pts);
  cv::Point2f GetPoint(int num);
  std::vector<cv::Point2f> GetPoints(int option);
};