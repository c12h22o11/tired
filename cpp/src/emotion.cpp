#include "emotion.hpp"

#include <cmath>

#include "config.hpp"
#include "spdlog/spdlog.h"

Emotion::Emotion() {
  EAR();
  MAR();
  Cal();
  SPDLOG_DEBUG("Constructed.");
}

Emotion::Emotion(std::vector<cv::Point2f> face_pts) : Face(face_pts) {
  Emotion();
}

Emotion::~Emotion() { SPDLOG_DEBUG("Destructed."); }

void Emotion::EAR() {
  double value[2], A, B, C;
  std::vector<cv::Point2f> eye;

  eye = GetPoints(1);
  A = Dist(eye[1], eye[5]);
  B = Dist(eye[2], eye[4]);
  C = Dist(eye[0], eye[3]);
  value[0] = (A + B) / (2.0 * C);

  eye = GetPoints(2);
  A = Dist(eye[1], eye[5]);
  B = Dist(eye[2], eye[4]);
  C = Dist(eye[0], eye[3]);
  value[1] = (A + B) / (2.0 * C);

  ear_ = (value[0] + value[1]) / 2;
}

void Emotion::MAR() {
  double A, B, C;
  std::vector<cv::Point2f> mouth = GetPoints(0);
  A = Dist(mouth[19], mouth[13]);
  B = Dist(mouth[17], mouth[15]);
  C = Dist(mouth[6], mouth[0]);
  mar_ = (A + B) / (2.0 * C);
}

bool Emotion::Cal() {
  int count_eye = 0;
  int count_mouth = 0;
  if (ear_ < thresh_eye_) {
    count_eye += 1;
  } else if (count_eye <= frame_eye_) {
    count_eye = 0;
  } else if (count_eye > thresh_eye_) {
    tired_ = true;
  }
  if (mar_ > thresh_mouth_) {
    count_mouth += 1;
  } else if (count_mouth != 0) {
    count_mouth = 0;
  }
  if (count_mouth < thresh_mouth_) {
    tired_ = true;
  }
  return tired_;
}

void Emotion::Thresh() {
  thresh_eye_ = 0.27;
  thresh_mouth_ = 0.18;
  frame_eye_ = 5;
  frame_mouth_ = 10;
}

double Emotion::Dist(cv::Point2f a, cv::Point2f b) {
  return sqrt(powf(a.x - b.x, 2) + powf(a.y - b.y, 2));
}

double Emotion::GetEAR() { return ear_; }

double Emotion::GetMAR() { return mar_; }

bool Emotion::GetTired() { return tired_; }

void Emotion::Draw(cv::Mat& frame) {
  std::ostringstream buf;
  int baseLine;
  int v_pos = 400;
  cv::Size text_size =
      cv::getTextSize(buf.str(), color::font, 1.0, 2, &baseLine);
  std::string str_mar = "The mouth is close", str_ear = "The eye is open",
              str_tired = "Not tired yet";
  std::vector<cv::Point2f> left_eye, right_eye, mouth;
  cv::convexHull(GetPoints(1), left_eye);
  cv::convexHull(GetPoints(2), right_eye);
  cv::convexHull(GetPoints(3), mouth);
  cv::drawContours(frame, left_eye, -1, color::green, 2);
  cv::drawContours(frame, right_eye, -1, color::green, 2);
  cv::drawContours(frame, mouth, -1, color::green, 2);

  if (GetEAR() < thresh_eye_) str_ear = "The eyes are too small";
  if (GetMAR() > thresh_mouth_) str_mar = "The mouth is too big";
  if (GetTired()) str_tired = "tired";

  buf << "EAR:\t" << GetEAR();
  cv::putText(frame, buf.str(), cv::Point(600, 440), color::font, 2,
              color::yellow);
              
  buf.str(std::string());
  buf << "MAR:\t" << GetMAR();
  cv::putText(frame, buf.str(), cv::Point(600, 420), color::font, 2,
              color::yellow);

  buf.str(std::string());
  buf << str_ear;
  v_pos -= static_cast<int>(1.3 * text_size.height);
  cv::putText(frame, buf.str(), cv::Point(0, v_pos), color::font, 1.0,
              color::green);

  buf.str(std::string());
  buf << str_mar;
  v_pos -= static_cast<int>(1.3 * text_size.height);
  cv::putText(frame, buf.str(), cv::Point(0, v_pos), color::font, 1.0,
              color::green);

  buf.str(std::string());
  buf << str_tired;
  v_pos -= static_cast<int>(1.3 * text_size.height);
  cv::putText(frame, buf.str(), GetPoint(8), color::font, 1.0, color::green);
}