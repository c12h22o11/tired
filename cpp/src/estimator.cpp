#include "estimator.hpp"

#include "config.hpp"

namespace {

const std::string json_path = "../../data/cam_cali.json";
const std::string test_cali = "../../image/test_cali.bmp";

std::vector<cv::Point3f> world_pts = {
    cv::Point3f(6.825897, 6.760612, 4.402142),
    cv::Point3f(1.330353, 7.122144, 6.903745),
    cv::Point3f(-1.330353, 7.122144, 6.903745),
    cv::Point3f(-6.825897, 6.760612, 4.402142),
    cv::Point3f(5.311432, 5.485328, 3.987654),
    cv::Point3f(1.789930, 5.393625, 4.413414),
    cv::Point3f(-1.789930, 5.393625, 4.413414),
    cv::Point3f(-5.311432, 5.485328, 3.987654),
    cv::Point3f(2.005628, 1.409845, 6.165652),
    cv::Point3f(-2.005628, 1.409845, 6.165652),
    cv::Point3f(2.774015, -2.080775, 5.048531),
    cv::Point3f(-2.774015, -2.080775, 5.048531),
    cv::Point3f(0.000000, -3.116408, 6.097667),
    cv::Point3f(0.000000, -7.415691, 4.070434)};

std::vector<cv::Point3f> projects_pts = {
    cv::Point3f(10.0, 10.0, 10.0),    cv::Point3f(10.0, 10.0, -10.0),
    cv::Point3f(10.0, -10.0, -10.0),  cv::Point3f(10.0, -10.0, 10.0),
    cv::Point3f(-10.0, 10.0, 10.0),   cv::Point3f(-10.0, 10.0, -10.0),
    cv::Point3f(-10.0, -10.0, -10.0), cv::Point3f(-10.0, -10.0, 10.0)};

int nums[] = {17, 21, 22, 26, 36, 39, 42, 45, 31, 35, 48, 54, 57, 8};

std::vector<cv::Point2i> line_pairs = {
    cv::Point2i(0, 1), cv::Point2i(1, 2), cv::Point2i(2, 3), cv::Point2i(3, 0),
    cv::Point2i(4, 5), cv::Point2i(5, 6), cv::Point2i(6, 7), cv::Point2i(7, 4),
    cv::Point2i(0, 4), cv::Point2i(1, 5), cv::Point2i(2, 6), cv::Point2i(3, 7)};

}  // namespace

Estimator::Estimator() : Face() {
  if (!LoadParams()) {
    cv::Mat cali_img = cv::imread(test_cali);
    Calibration(cali_img);
    LoadParams();
  }
  SPDLOG_DEBUG("Estimator construted.");
}

void Estimator::SetEstimator(std::vector<cv::Point2f> face_pts) {
  SetFace(face_pts);
  image_pts_.clear();
  for (int i = 0; i < 14; i++) image_pts_.emplace_back(GetPoint(nums[i]));
  SPDLOG_DEBUG("success SetEstimator");
}

Estimator::~Estimator() { SPDLOG_DEBUG("Estimator destructed."); }

std::vector<cv::Point2f> Estimator::estimator() {
  std::vector<cv::Point2f> image_pts;
  std::vector<double> euler;
  cv::Mat rotation_vector(3, 3, CV_32FC1), pose(3, 2, CV_32FC1);

  cv::solvePnP(world_pts, image_pts_, cam_mat_, dis_coff_, rotation_,
               translation_, false, cv::SOLVEPNP_IPPE);

  cv::projectPoints(projects_pts, rotation_, translation_, cam_mat_, dis_coff_,
                    image_pts);

  cv::Rodrigues(rotation_, rotation_vector);

  cv::hconcat(rotation_vector, translation_, pose);
  SPDLOG_DEBUG("cols rows\n Rvec: {} {}, \nR: {} {},\nT: {} {}",
               rotation_vector.cols(), rotation_vector.rows(), rotation_.cols(),
               rotation_.rows(), translation_.cols(), translation_.rows());

  cv::decomposeProjectionMatrix(pose, cam_mat_, rotation_vector, translation_,
                                cv::noArray(), cv::noArray(), cv::noArray(),
                                euler);

  euler_angle_.pitch = euler[0];
  euler_angle_.yaw = euler[1];
  euler_angle_.roll = euler[2];
  return image_pts;
}

void Estimator::Draw(cv::Mat& frame, std::vector<cv::Point2f> image_pts) {
  std::ostringstream buf;
  int baseLine;
  int v_pos = 0;
  cv::Size text_size;

  buf << "pitch:\t" << euler_angle_.pitch;
  text_size = cv::getTextSize(buf.str(), color::font, 1.0, 2, &baseLine);
  v_pos += static_cast<int>(1.3 * text_size.height);
  cv::putText(frame, buf.str(), cv::Point(0, v_pos), color::font, 1.0,
              color::green);

  buf.str(std::string());
  buf << "yaw:\t" << euler_angle_.yaw;
  text_size = cv::getTextSize(buf.str(), color::font, 1.0, 2, &baseLine);
  v_pos += static_cast<int>(1.3 * text_size.height);
  cv::putText(frame, buf.str(), cv::Point(0, v_pos), color::font, 1.0,
              color::green);

  buf.str(std::string());
  buf << "roll:\t" << euler_angle_.roll;
  text_size = cv::getTextSize(buf.str(), color::font, 1.0, 2, &baseLine);
  v_pos += static_cast<int>(1.3 * text_size.height);
  cv::putText(frame, buf.str(), cv::Point(0, v_pos), color::font, 1.0,
              color::green);

  for (auto& point : line_pairs) {
    cv::line(frame, image_pts[point.x], image_pts[point.y], color::green, 2);
  }
}

bool Estimator::LoadParams() {
  cv::FileStorage fs(json_path,
                     cv::FileStorage::READ | cv::FileStorage::FORMAT_JSON);
  if (fs.isOpened()) {
    cam_mat_ = fs["cam_mat"].mat();
    dis_coff_ = fs["dis_coff"].mat();
    rotation_ = fs["rotation"].mat();
    translation_ = fs["translation"].mat();
    fs.release();
    SPDLOG_DEBUG("success load cam_cali.json.");
    return true;
  } else {
    SPDLOG_ERROR("Can not load cam_cali.json.");
    return false;
  }
}

void Estimator::Calibration(cv::Mat view) {
  cv::Size image_size, board_size(9, 6),
      win_size(5, 5);       // win_size一般无需修改
  float square_size = 14.,  // 14mm
      grid_width = square_size * (board_size.width - 1);  //左上角到右上角距离
  cv::Mat view_gray, camera_matrix = cv::Mat::eye(3, 3, CV_64F),
                     dist_coeffs = cv::Mat::zeros(8, 1, CV_64F);
  std::vector<cv::Mat> rvecs, tvecs;
  std::vector<cv::Point2f> point_buf;
  std::vector<std::vector<cv::Point2f>> image_points;
  std::vector<cv::Point3f> corners, newObj_points;
  std::vector<std::vector<cv::Point3f>> object_points(1);

  bool found;
  double rms;
  const int fixed_point = -1;
  SPDLOG_DEBUG("start calirate");
  if (view.empty()) {
    SPDLOG_WARN("view is empty!!!");
    return;
  }
  image_size = view.size();
  cv::cvtColor(view, view_gray, cv::COLOR_BGR2GRAY);

  found = findChessboardCorners(view, board_size, point_buf,
                                cv::CALIB_CB_ADAPTIVE_THRESH |
                                    cv::CALIB_CB_FAST_CHECK |
                                    cv::CALIB_CB_NORMALIZE_IMAGE);
  if (!found) {
    SPDLOG_WARN("Can't Found Chessboard Corners!");
    return;
  }
  cv::cornerSubPix(
      view_gray, point_buf, win_size, cv::Size(-1, -1),
      cv::TermCriteria(cv::TermCriteria::EPS + cv::TermCriteria::COUNT, 30,
                       0.0001));
  image_points.push_back(point_buf);

  for (int i = 0; i < board_size.height; i++) {
    for (int j = 0; j < board_size.width; j++) {
      corners.push_back(
          cv::Point3f(float(j * square_size), float(i * square_size), 0));
    }
  }
  object_points[0] = corners;
  object_points[0][board_size.width - 1].x = object_points[0][0].x + grid_width;
  newObj_points = object_points[0];
  object_points.resize(image_points.size(), object_points[0]);
  rms = cv::calibrateCameraRO(object_points, image_points, image_size,
                              fixed_point, camera_matrix, dist_coeffs, rvecs,
                              tvecs, newObj_points,
                              cv::CALIB_FIX_K3 | cv::CALIB_USE_LU);
  SPDLOG_DEBUG("RMS error reported by calibrateCamera: {}", rms);
  object_points.clear();
  object_points.resize(image_points.size(), newObj_points);
  /*
    cv::Mat bigmat((int)rvecs.size(), 6,
                   rvecs[0].type());  //先旋转向量 后平移向量
    if (!rvecs.empty() && !tvecs.empty()) {
      CV_Assert(rvecs[0].type() == tvecs[0].type());
      for (int i = 0; i < (int)rvecs.size(); i++) {
        cv::Mat r = bigmat(cv::Range(i, i + 1), cv::Range(0, 3));
        cv::Mat t = bigmat(cv::Range(i, i + 1), cv::Range(3, 6));

        CV_Assert(rvecs[i].rows == 3 && rvecs[i].cols == 1);
        CV_Assert(tvecs[i].rows == 3 && tvecs[i].cols == 1);
        //*.t() is MatExpr (not Mat) so we can use assignment operator
        r = rvecs[i].t();
        t = tvecs[i].t();
      }
      // cvWriteComment( *fs, "a set of 6-tuples (rotation vector + translation
      // vector) for each view", 0 );
    }
  */
  cv::FileStorage fs(json_path,
                     cv::FileStorage::WRITE | cv::FileStorage::FORMAT_JSON);
  fs << "cam_mat" << cam_mat_;
  fs << "dis_coff" << dis_coff_;
  fs << "rotation" << rotation_;
  fs << "translation" << translation_;
  fs.release();
  SPDLOG_DEBUG("Calirate successly.");
  return;
}