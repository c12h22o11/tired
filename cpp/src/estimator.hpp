#pragma once

#include <vector>

#include "face.hpp"
#include "opencv2/opencv.hpp"
#include "spdlog/spdlog.h"

class Estimator : private Face {
 private:
  cv::Mat cam_mat_;
  cv::Mat dis_coff_;
  cv::Mat rotation_;
  cv::Mat translation_;

  std::vector<cv::Point2f> image_pts_;

  struct euler_angle {
    double yaw;
    double pitch;
    double roll;
  } euler_angle_;

  void Calibration(cv::Mat view);
  bool LoadParams();

 public:
  Estimator();
  void SetEstimator(std::vector<cv::Point2f> face_pts);
  ~Estimator();

  std::vector<cv::Point2f> estimator();

  void Draw(cv::Mat& frame, std::vector<cv::Point2f> image_pts);
};