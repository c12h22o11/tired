#pragma once

#include "opencv2/opencv.hpp"

namespace dict {
static const cv::Point2i mouth = cv::Point2i(48, 68);
static const cv::Point2i right_eyebrow = cv::Point2i(17, 22);
static const cv::Point2i left_eyebrow = cv::Point2i(22, 27);
static const cv::Point2i right_eye = cv::Point2i(36, 42);
static const cv::Point2i left_eye = cv::Point2i(42, 48);
static const cv::Point2i nose = cv::Point2i(27, 36);
static const cv::Point2i jaw = cv::Point2i(0, 17);
}  // namespace dict

namespace color {

static const auto red = cv::Scalar(0, 0, 255);
static const auto green = cv::Scalar(0, 255, 0);
static const auto yellow = cv::Scalar(0, 255, 255);

static const auto font = cv::FONT_HERSHEY_COMPLEX;

}  // namespace color