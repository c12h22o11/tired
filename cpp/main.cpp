#include "dlib/gui_widgets.h"
#include "dlib/image_processing.h"
#include "dlib/image_processing/frontal_face_detector.h"
#include "dlib/image_processing/render_face_detections.h"
#include "dlib/opencv.h"

#include "emotion.hpp"
#include "estimator.hpp"

#include "opencv2/opencv.hpp"

#include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/spdlog.h"

namespace {

const std::string path_video = "../../image/test.avi";
const std::string path_image = "../../image/test.jpg";
const std::string dat_name = "../../data/shape_predictor_68_face_landmarks.dat";

}  // namespace

int main() {
  auto console_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
  auto file_sink = std::make_shared<spdlog::sinks::basic_file_sink_mt>(
      "logs/tired.log", true);

  spdlog::sinks_init_list sink_list = {console_sink, file_sink};

  spdlog::set_default_logger(
      std::make_shared<spdlog::logger>("default", sink_list));

#if (SPDLOG_ACTIVE_LEVEL == SPDLOG_LEVEL_DEBUG)
  spdlog::flush_on(spdlog::level::debug);
  spdlog::set_level(spdlog::level::debug);
#elif (SPDLOG_ACTIVE_LEVEL == SPDLOG_LEVEL_INFO)
  spdlog::flush_on(spdlog::level::err);
  spdlog::set_level(spdlog::level::info);
#endif

  SPDLOG_WARN("***** Running Tired. *****");

  dlib::frontal_face_detector detector = dlib::get_frontal_face_detector();
  dlib::shape_predictor sp;

  dlib::deserialize(dat_name) >> sp;
  cv::Mat temp;
  cv::VideoCapture capture;
  std::vector<cv::Point2f> face_pts;
  std::vector<dlib::full_object_detection> shapes;

#define x 1
#if x
  cv::Mat image = cv::imread(path_image);
  temp = image;
#elif 0
  cv::VideoCapture cap(path_video);
  capture = cap;
#else
  cv::VideoCapture cap(0);
  capture = cap;
#endif

  try {
#if x
    if (temp.empty()) {
      SPDLOG_ERROR("Can't get the image.");
      return 1;
    }
#else
    if (!capture.isOpened()) {
      SPDLOG_ERROR("Can't open capture.");
      return 1;
    }
#endif

    SPDLOG_WARN("all res loaded");

    Estimator estimator;

    for (;;) {
#if !x
      capture >> temp;
#endif
#undef x

      cv::resize(temp, temp, cv::Size(720, 540));

      cv::imshow("show", temp);
      if (27 == cv::waitKey(1)){
        SPDLOG_WARN("program will quit, the esc was pressed");
        return 0;
      }

      dlib::cv_image<dlib::bgr_pixel> cimg(temp);
      std::vector<dlib::rectangle> faces = detector(cimg);
      SPDLOG_DEBUG("step is one");
      for (auto& face : faces) {
        shapes.push_back(sp(cimg, face));
      }

      if (!shapes.empty()) {
        for (auto& shape : shapes) {
          for (int i; i < 68; i++) {
            face_pts.push_back(cv::Point2f((float)shape.part(i).x(),
                                           (float)shape.part(i).y()));
          }
        }
        SPDLOG_DEBUG("step is two");
        estimator.SetEstimator(face_pts);
        SPDLOG_DEBUG("step is three");
        //estimator.Draw(temp, estimator.estimator());
      }
      SPDLOG_DEBUG("all step is over");
      cv::imshow("show", temp);
      if (27 == cv::waitKey(1)){ // esc == 27
        SPDLOG_WARN("program will quit, the esc was pressed");
        return 0;
      }
    }

  } catch (cv::Exception& e) {
    std::cout << e.what() << std::endl;
  }
  return 0;
}